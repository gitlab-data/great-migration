WITH milestones AS (

  SELECT *
  FROM analytics.gitlab_dotcom_milestones

), merge_requests AS  (

  SELECT *
  FROM analytics.gitlab_dotcom_merge_requests_xf

), filtered_merge_requests AS (

  SELECT merge_request_id,
         author_id,
         target_project_id,
         milestone_id
  FROM merge_requests
  WHERE is_community_contributor_related = TRUE
  AND merge_request_state = 'merged'

  )

SELECT filtered_merge_requests.milestone_id,
       start_date,
       due_date,
       COUNT(DISTINCT author_id) AS author_distinct_count,
       COUNT(DISTINCT merge_request_id) AS merge_request_distinct_count
FROM filtered_merge_requests
LEFT JOIN milestones
ON filtered_merge_requests.milestone_id = milestones.milestone_id
WHERE start_date > DATEADD('year', -1, CURRENT_TIMESTAMP())
GROUP BY 1, 2, 3
ORDER BY 2