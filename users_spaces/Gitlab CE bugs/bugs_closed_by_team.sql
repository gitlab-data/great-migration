with issues as (

    SELECT * FROM analytics.gitlab_dotcom_issues_xf

), users as (

    SELECT * FROM analytics.gitlab_dotcom_users_xf

), joined as (

    SELECT issue_id,
           organization,
           closed_at
    FROM issues
    LEFT JOIN users
    ON issues.author_id = users.user_id
    WHERE
    project_id = '13083'
    AND masked_label_title LIKE '%bug%'
)

SELECT closed_at,
       organization,
       count(issue_id) as closed_issues_count
FROM joined
WHERE closed_at IS NOT NULL
GROUP BY 1, 2