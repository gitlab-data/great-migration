with source as (

	SELECT * FROM analytics.sfdc_opportunity_xf

)

SELECT sum(incremental_acv)
FROM source
WHERE MAPPED_STAGE NOT IN ('0-Pending Acceptance', 'Duplicate', 
							'Unmapped', 'Unqualified')