with source as (

	SELECT * FROM analytics.sfdc_opportunity_xf
)

SELECT sum(incremental_acv)
FROM source
WHERE mapped_stage NOT IN ('0-Pending Acceptance','1-Discovery',
							'2-Scoping','3-Technical Evaluation', 
							'Duplicate', 'Unmapped', 'Unqualified')
