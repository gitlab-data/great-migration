with opportunities as (

  SELECT * FROM analytics.sfdc_opportunity_xf 
  WHERE is_won = True
  AND close_date >= '2019-02-01'

), goals as (

  SELECT * FROM analytics.sheetload_iacv_goal

), dates as (

    SELECT * FROM analytics.date_details 
    WHERE date_actual >= '2019-02-01'
    AND date_actual < '2019-05-01'

), joined as (

    SELECT dates.date_actual,
            dates.fy_day_of_quarter,
            dates.fy_first_day_of_quarter,
            sum(opportunities.incremental_acv) as iacv
    FROM dates
    LEFT JOIN opportunities
    ON dates.date_actual::date = opportunities.close_date::date
    GROUP BY 1, 2, 3

), goal_transformation as (

    SELECT date_details.fy_first_day_of_quarter as date_day,
            sum(iacv_goal) as iacv_goal
    FROM analytics.sheetload_iacv_goal
    LEFT JOIN analytics.date_details
    ON date_day::date = date_actual::date
    GROUP BY 1

)

SELECT date_actual,
        fy_day_of_quarter,
        fy_first_day_of_quarter,
        max(iacv_goal) OVER ( PARTITION BY fy_first_day_of_quarter ORDER BY date_actual) as iacv_goal_for_fq,
        max(fy_day_of_quarter) OVER ( PARTITION BY fy_first_day_of_quarter) as days_in_fiscal_quarter,
        iacv_goal_for_fq/days_in_fiscal_quarter as target_per_day,
        target_per_day*fy_day_of_quarter as target_trend,
       iacv,
       CASE WHEN date_actual < CURRENT_DATE THEN (sum(iacv) OVER ( PARTITION BY fy_first_day_of_quarter ORDER BY date_actual))
            ELSE NULL END as iacv_running_total
FROM joined
LEFT JOIN goal_transformation
ON joined.fy_first_day_of_quarter = goal_transformation.date_day
ORDER BY 1